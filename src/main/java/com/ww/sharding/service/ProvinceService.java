package com.ww.sharding.service;

import com.ww.sharding.model.Province;

public interface ProvinceService {

    public void save(Province province);

}
