package com.ww.sharding.service;

import com.ww.sharding.model.Order;
import com.ww.sharding.model.OrderItem;

import java.util.List;

public interface OrderService {

    public List<Order> findHint();

    public void save(Order order, OrderItem item);

}
