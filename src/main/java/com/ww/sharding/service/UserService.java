package com.ww.sharding.service;

import com.ww.sharding.model.User;

public interface UserService {

    public void save(User user);

}
