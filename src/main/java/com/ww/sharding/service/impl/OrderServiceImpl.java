package com.ww.sharding.service.impl;

import com.ww.sharding.mapper.OrderItemMapper;
import com.ww.sharding.mapper.OrderMapper;
import com.ww.sharding.model.Order;
import com.ww.sharding.model.OrderItem;
import com.ww.sharding.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Override
    public void save(Order order, OrderItem item) {
        orderMapper.save(order);
        orderItemMapper.save(item);
    }

    @Override
    public List<Order> findHint() {
        return orderMapper.selectHint();
    }
}
