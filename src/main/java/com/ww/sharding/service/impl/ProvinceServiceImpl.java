package com.ww.sharding.service.impl;

import com.ww.sharding.mapper.ProvinceMapper;
import com.ww.sharding.model.Province;
import com.ww.sharding.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinceServiceImpl implements ProvinceService {
    @Autowired
    private ProvinceMapper provinceMapper;

    @Override
    public void save(Province province) {
        provinceMapper.save(province);
    }
}
