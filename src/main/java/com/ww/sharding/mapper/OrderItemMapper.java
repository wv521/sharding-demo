package com.ww.sharding.mapper;

import com.ww.sharding.model.OrderItem;

public interface OrderItemMapper {
    public int save(OrderItem orderItem);
}
