package com.ww.sharding.mapper;

import com.ww.sharding.model.Province;

public interface ProvinceMapper {
    public int save(Province province);
}
