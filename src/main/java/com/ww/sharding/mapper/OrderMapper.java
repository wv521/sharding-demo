package com.ww.sharding.mapper;

import com.ww.sharding.model.Order;

import java.util.List;

public interface OrderMapper {
    public int save(Order order);

    public List<Order> selectHint();
}
