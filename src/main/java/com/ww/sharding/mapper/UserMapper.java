package com.ww.sharding.mapper;

import com.ww.sharding.model.User;

public interface UserMapper {
    public int save(User user);
}
